$(document).ready(function(){

    (function($) {
        "use strict";


    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate registrationForm form
    $(function() {
        $('#registrationForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: "Please, give us a name, it doesn't have to be real.",
                    minlength: "your name must consist of at least 2 characters"
                },
                email: {
                    required: "we need an email to give you information about the event"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"registration_process.php",
                    success: function() {
                        $('#registrationForm :input').attr('disabled', 'disabled');
                        $('#registrationForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                        $('#success').modal('show');
                        })
                    },
                    error: function() {
                        $('#registrationForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                        $('#error').modal('show');
                        })
                    }
                })
            }
        })
    })

 })(jQuery)
})
